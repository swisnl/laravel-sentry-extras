<?php

namespace Swis\Laravel\SentryExtras;

use Sentry\SentrySdk;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class SentryExtrasServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('laravel-sentry-extras')
            ->hasConfigFile();
    }

    public function packageBooted(): void
    {
        /** @var \Swis\Laravel\SentryExtras\SentryExtras $extras */
        $extras = $this->app->make(SentryExtras::class);
        SentrySdk::getCurrentHub()->configureScope([$extras, 'configureScope']);
    }
}
