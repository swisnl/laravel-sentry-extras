<?php

namespace Swis\Laravel\SentryExtras;

use Sentry\State\Scope;

class SentryExtras
{
    public function configureScope(Scope $scope): Scope
    {
        return $scope->setContext('Kubernetes', $this->getCluster());
    }

    protected function getCluster(): array
    {
        return array_filter([
            'Cluster' => config('sentry-extras.cluster.name'),
            'Pool' => config('sentry-extras.cluster.pool_name'),
            'Node' => config('sentry-extras.cluster.node_name'),
            'Pod' => config('sentry-extras.cluster.pod_name'),
            'Namespace' => config('sentry-extras.cluster.namespace'),
        ]);
    }
}
