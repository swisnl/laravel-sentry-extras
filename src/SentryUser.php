<?php

namespace Swis\Laravel\SentryExtras;

use Sentry\Event;
use Sentry\State\Scope;
use Sentry\UserDataBag;
use Throwable;

class SentryUser
{
    /**
     * The user resolver.
     *
     * @var callable(): (array|\Sentry\UserDataBag|null)
     */
    protected $resolver;

    /**
     * Create a new Sentry user callback instance.
     *
     * @param  callable(): (array|\Sentry\UserDataBag|null)  $resolver  the user resolver
     * @return void
     */
    public function __construct(callable $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * Execute the user data callback.
     *
     * @param  \Sentry\State\Scope  $scope  the sentry scope instance
     * @return void
     */
    public function __invoke(Scope $scope)
    {
        $scope->addEventProcessor(function (Event $event): Event {
            try {
                $resolver = $this->resolver;

                if ($user = $resolver()) {
                    if (\is_array($user)) {
                        $user = UserDataBag::createFromArray($user);
                    }
                    $event->setUser($user);
                }
            } catch (Throwable $e) {
                // Ignore any errors.
            }

            return $event;
        });
    }
}
