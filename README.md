# Extra debugging information in Sentry, specifically for SWIS

[![Latest Version on Packagist](https://img.shields.io/packagist/v/swisnl/laravel-sentry-extras.svg?style=flat-square)](https://packagist.org/packages/swisnl/laravel-sentry-extras)
[![Total Downloads](https://img.shields.io/packagist/dt/swisnl/laravel-sentry-extras.svg?style=flat-square)](https://packagist.org/packages/swisnl/laravel-sentry-extras)

## Installation/usage

Just install the package via composer, and you're good to go:

```bash
composer require swisnl/laravel-sentry-extras
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

If you discover any security related issues, please email security@swis.nl instead of using the issue tracker.

## Credits

- [Jasper Zonneveld](https://github.com/JaZo)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
