# Changelog

All notable changes to `swisnl/laravel-sentry-extras` will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

* Nothing

## [1.3.0] - 2025-02-26

### Added

* Added support for Laravel 12.

### Removed

* Removed support for PHP 7.

## [1.2.0] - 2024-03-28

### Added

* Added support for Sentry 4.
* Added support for Laravel 11.

## [1.1.2] - 2023-09-12

### Fixed

* Removed Bugsnag references.

## [1.1.1] - 2023-09-06

### Fixed

* Do not remove user if resolver returns nothing.

## [1.1.0] - 2023-09-06

### Added

* Added Kubernetes pod name and namespace.
